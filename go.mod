module github.com/crowdsecurity/go-cs-bouncer

go 1.15

require (
	github.com/antonmedv/expr v1.9.0 // indirect
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/crowdsecurity/crowdsec v1.4.1
	github.com/crowdsecurity/grokky v0.1.0 // indirect
	github.com/go-openapi/errors v0.20.3 // indirect
	github.com/go-openapi/loads v0.21.2 // indirect
	github.com/go-openapi/spec v0.20.7 // indirect
	github.com/go-openapi/swag v0.22.1 // indirect
	github.com/go-openapi/validate v0.22.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-version v1.6.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/prometheus/client_golang v1.13.0
	github.com/sirupsen/logrus v1.9.0
	go.mongodb.org/mongo-driver v1.10.1 // indirect
	golang.org/x/net v0.0.0-20220812174116-3211cb980234 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	gopkg.in/yaml.v2 v2.4.0
)
