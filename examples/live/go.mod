module github.com/crowdsecurity/go-cs-bouncer/examples/live

go 1.14

replace github.com/crowdsecurity/go-cs-bouncer => ../../

require github.com/crowdsecurity/go-cs-bouncer v0.0.0-20220316142326-5af635d49c23
