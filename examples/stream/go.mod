module github.com/crowdsecurity/go-cs-bouncer/examples/stream

replace github.com/crowdsecurity/go-cs-bouncer => ../../

go 1.14

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/armon/consul-api v0.0.0-20180202201655-eb2c6b5be1b6 // indirect
	github.com/crowdsecurity/crowdsec v1.3.3
	github.com/crowdsecurity/go-cs-bouncer v0.0.0-20220316142326-5af635d49c23
	github.com/go-bindata/go-bindata v1.0.1-0.20190711162640-ee3c2418e368 // indirect
	github.com/go-kit/kit v0.10.0 // indirect
	github.com/jamiealquiza/tachymeter v2.0.0+incompatible // indirect
	github.com/jinzhu/gorm v1.9.12 // indirect
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/logrusorgru/grokky v0.0.0-20180829062225-47edf017d42c // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rogpeppe/godef v1.1.2 // indirect
	github.com/sevlyar/go-daemon v0.1.5 // indirect
	github.com/xordataexchange/crypt v0.0.3-0.20170626215501-b2862e3d0a77 // indirect
)
